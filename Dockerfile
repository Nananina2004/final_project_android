FROM cirrusci/android-sdk:30

WORKDIR /app

COPY . /app

RUN apt-get update && \
    apt-get install -y openjdk-17-jdk

ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64

RUN chmod +x ./gradlew && ./gradlew assembleDebug
